﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text.Json;
using System.Linq;
using System.IO;
using System.Globalization;

namespace ProSqlServer
{
    public class Startup
    {
        /*readonly string corsPolicy = "_corsPolicy";*/

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            /*services.AddCors(options =>
            {
                options.AddPolicy(name: corsPolicy,
                                  builder =>
                                  {
                                      builder.AllowAnyMethod()
                                             .AllowAnyHeader()
                                             .SetIsOriginAllowed(origin => true)
                                             .AllowCredentials();
                                  });
            });*/

            // services.AddResponseCaching();
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseCors(builder =>
            {
                builder.AllowAnyMethod()
                       .AllowAnyHeader()
                       .SetIsOriginAllowed(origin => true)
                       .AllowCredentials();
            });

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapGet("/", async context =>
                {
                    await context.Response.WriteAsync("Working Fine..!!");
                });

                // Get table names
                endpoints.MapGet("/api/tables", async context => 
                {
                    string connectionString = context.Request.Query["connectionString"][0];
                    List<String> tableNames = new();
                    try { 
                        using (SqlConnection _connection = new(connectionString))
                        {
                            _connection.Open();

                            String tableName;
                            DataTable dt = _connection.GetSchema("Tables");
                            
                            foreach (DataRow row in dt.Rows)
                            {
                                tableName = row[2].ToString();
                                tableNames.Add(tableName);
                            }
                        }
                        await context.Response.WriteAsJsonAsync(JsonSerializer.Serialize(tableNames));
                    }
                    catch (ArgumentException exception)
                    {
                        context.Response.StatusCode = 409;
                        await context.Response.WriteAsync(exception.Message);
                    }
                    catch (SqlException exception)
                    {
                        context.Response.StatusCode = 406;
                        await context.Response.WriteAsync(exception.Message);
                    }
                    catch (Exception exception)
                    {
                        context.Response.StatusCode = 500;
                        await context.Response.WriteAsync(exception.Message);
                    }
                });

                // Get selected table data
                endpoints.MapGet("/api/table", async context =>
                {
                    string connectionString = context.Request.Query["connectionString"][0];
                    string tableName = context.Request.Query["tableName"][0];
                    string escapedTableName = new SqlCommandBuilder().QuoteIdentifier(tableName);
                    string identityColumnName = null;
                    List<List<string>> tableData = new();
                    
                    try { 
                        using (SqlConnection conn = new(connectionString))
                        {
                            conn.Open();
                            
                            String query = "SELECT TOP 0 * FROM " + escapedTableName;
                            SqlCommand sqlCommand = new(query, conn);
                            
                            using (SqlDataReader reader = sqlCommand.ExecuteReader())
                            {
                                DataTable schemaTable = reader.GetSchemaTable();
                                foreach (DataRow col in schemaTable.Rows)
                                {
                                    if (col.Field<bool>("IsIdentity") == true)
                                    {
                                        identityColumnName = col.Field<string>("ColumnName");
                                        break;
                                    }
                                }
                            }
                            using (SqlDataAdapter adapter = new())
                            {
                                sqlCommand.CommandText = "SELECT * FROM " + escapedTableName;
                                adapter.SelectCommand = sqlCommand;
                                
                                DataTable dt = new();
                                adapter.Fill(dt);
                                
                                List<String> columnList = dt.Columns.Cast<DataColumn>()
                                     .Select(x => x.ColumnName)
                                     .ToList();
                                tableData.Add(columnList);
                                
                                foreach (DataRow row in dt.Rows)
                                {
                                    List<string> rows = new();
                                    foreach (var item in row.ItemArray)
                                    {
                                        if (item.GetType().Equals(typeof(DateTime)))
                                        {
                                            var x = DateTime.ParseExact(item.ToString(), "dd-MM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                                            rows.Add(x.ToString("yyyy-MM-dd HH:mm:ss"));
                                        }
                                        else
                                        {
                                            rows.Add(item.ToString());
                                        }
                                    }
                                    tableData.Add(rows);
                                }
                            }
                        }
                        Dictionary<string, dynamic> responseBody = new();
                        responseBody.Add("identityColumnName", identityColumnName);
                        responseBody.Add("tableData", tableData);
                        
                        await context.Response.WriteAsJsonAsync(JsonSerializer.Serialize(responseBody));
                    }
                    catch (Exception exception)
                    {
                        context.Response.StatusCode = 500;
                        await context.Response.WriteAsync(exception.Message);
                    }
                });

                // Add a new table row
                endpoints.MapPost("/api/table", async context => 
                {
                    string connectionString = context.Request.Query["connectionString"][0];
                    string tableName = context.Request.Query["tableName"][0];
                    string escapedTableName = new SqlCommandBuilder().QuoteIdentifier(tableName);
                    Dictionary<string,string> bodyParams = await JsonSerializer.DeserializeAsync<Dictionary<string, string>>(context.Request.Body);

                    using (SqlConnection conn = new(connectionString))
                    {
                        conn.Open();
                        
                        string query = "INSERT INTO " + escapedTableName + "(" + string.Join(",", bodyParams.Keys) + ") VALUES (@" + string.Join(",@", bodyParams.Keys) + ")";
                        SqlCommand sqlCommand = new(query, conn);
                        
                        foreach (var key in bodyParams.Keys)
                        {
                            sqlCommand.Parameters.AddWithValue("@" + key, bodyParams[key]?.Length == 0 ? DBNull.Value : bodyParams[key]);
                        }
                        try
                        {
                            int rowsAffected = sqlCommand.ExecuteNonQuery();

                            Dictionary<string, string> responseBody = new();
                            responseBody.Add("message", $"✓ Success ({rowsAffected} row{(rowsAffected != 1 ? "s" : "")} affected)");
                            
                            context.Response.StatusCode = 200;
                            await context.Response.WriteAsJsonAsync(JsonSerializer.Serialize(responseBody));
                        }
                        catch(SqlException exception)
                        {
                            context.Response.StatusCode = 409;
                            await context.Response.WriteAsync(exception.Message);
                        }
                        catch (Exception exception)
                        {
                            context.Response.StatusCode = 500;
                            await context.Response.WriteAsync(exception.Message);
                        }
                    }
                });

                // Edit values of a table row
                endpoints.MapPut("/api/table", async context => 
                {
                    string connectionString = context.Request.Query["connectionString"][0];
                    string tableName = context.Request.Query["tableName"][0];
                    string escapedTableName = new SqlCommandBuilder().QuoteIdentifier(tableName);
                    IDictionary<string, IDictionary<string, string>> bodyParams = await JsonSerializer.DeserializeAsync<IDictionary<string, IDictionary<string, string>>>(context.Request.Body);

                    using (SqlConnection conn = new(connectionString))
                    {
                        conn.Open();

                        string query = "UPDATE " + escapedTableName + " SET ";

                        foreach (var key in bodyParams["newRowValues"].Keys)
                        {
                            query += key + "=@new" + key + ",";
                        }
                        query = query.Remove(query.Length - 1, 1) + " WHERE ";

                        foreach (var key in bodyParams["oldRowValues"].Keys)
                        {
                            if (bodyParams["oldRowValues"][key]?.Length == 0)
                                query += "(" + key + " is null OR "+key+" = \'\' )"+ " AND ";
                            else
                                query += key + "=@old" + key + " AND ";
                        }
                        query = query.Remove(query.Length - 5, 5);

                        SqlCommand sqlCommand = new(query, conn);

                        foreach (var key in bodyParams["newRowValues"].Keys)
                        {
                            sqlCommand.Parameters.AddWithValue("@new" + key, bodyParams["newRowValues"][key]?.Length == 0 ? DBNull.Value : bodyParams["newRowValues"][key]);
                        }

                        foreach (var key in bodyParams["oldRowValues"].Keys)
                        {
                            if (bodyParams["oldRowValues"][key]?.Length != 0)
                                sqlCommand.Parameters.AddWithValue("@old" + key, bodyParams["oldRowValues"][key]);
                            
                        }

                        try
                        {
                            int rowsAffected = sqlCommand.ExecuteNonQuery();

                            Dictionary<string, string> responseBody = new();
                            responseBody.Add("message", $"✓ Success ({rowsAffected} row{(rowsAffected != 1 ? "s" : "")} affected)");
                            
                            context.Response.StatusCode = 200;
                            await context.Response.WriteAsJsonAsync(JsonSerializer.Serialize(responseBody));
                        }
                        catch (SqlException exception)
                        {
                            context.Response.StatusCode = 409;
                            await context.Response.WriteAsync(exception.Message);
                        }
                        catch (Exception exception)
                        {
                            context.Response.StatusCode = 500;
                            await context.Response.WriteAsync(exception.Message);
                        }
                    }
                });
            });
        }
    }
}
